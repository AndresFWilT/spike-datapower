import {ISoapRequest, SoapService as Soap} from '@npm-davi/davi-coe-connection-http-service-nodejs-lib/src/index.js';
import { IUserQueryFilters, ISoapResponse } from '../../../domain/interfaces';
import { DataPowerRepository } from '../../../domain/repositories/dataPower.repository';

import config from '../../../config';

export class DataPowerImpl implements DataPowerRepository {

    constructor() { }

    public async fetchCustomerCrucialData(
        UUID: string,
        body: IUserQueryFilters,
        url: string,
        contract: string
    ): Promise<ISoapResponse> {

        try {
            const data: ISoapRequest = {
                UUID,
                wsdlPath: `/src/static/SOAP/${contract}/${contract}.wsdl`,
                url: url,
                privateKeyPath: `/src/static/SOAP/${contract}/cerKey.key`,
                publicKeyPath: `/src/static/SOAP/${contract}/certificate.cer`
            };
            await Soap.initializeSoapClient(data);
            return await Soap.callSoapMethod(body);

        } catch (error) {
            throw new Error(`ERROR SERVICE: in executing SOAP call - Details: ${error}`);
        }
    }

}
