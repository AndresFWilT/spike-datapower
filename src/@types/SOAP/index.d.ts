declare module '@npm-davi/davi-coe-connection-http-service-nodejs-lib/src/index.js' {
    import { ISoapCallMethod } from '../../interfaces/ISoapCallMethod';

    export interface IFetchRequest {
    }

    export interface ISoapRequest {

    }

    export class FetchService {
        public static async request<T>(fetchRequestClient: IFetchRequest): Promise<T>;
    }

    export class SoapService {
        public static async initializeSoapClient(soapRequest: ISoapRequest): Promise<void>;

        public static async callSoapMethod<T>(callMethod: ISoapCallMethod): Promise<T>;
    }

}