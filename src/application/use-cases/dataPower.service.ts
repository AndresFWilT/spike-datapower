import debugLib from 'debug';

import {
    IUserQueryFilters,
    IRequestHeadersCustom,
    ISoapResponse,
} from '../../domain/interfaces';
import { DataPowerRepository } from '../../domain/repositories/dataPower.repository';

import config from '../../config';

const debug = debugLib('davi:dataPower.service');

export class DataPowerService {

    constructor(
        private readonly dataPowerRepository: DataPowerRepository,
    ) { }

    public async getIntentionAIResponse(
        UUID: string,
        body: IUserQueryFilters,
        headers: IRequestHeadersCustom
    ): Promise<ISoapResponse> {

        debug(`UUID: ${UUID} - Call to dataPower service - body: ${JSON.stringify(body)} - Headers: ${JSON.stringify(headers)} - Body:  ${JSON.stringify(body)} `);
        try {

            const data = await this.dataPowerRepository.fetchCustomerCrucialData(UUID, body, 'headers', 'SrvScnUsuario');

            debug(`UUID: ${UUID} - Response DataPower service: ${JSON.stringify(data)}`);
            return data;

        } catch (error) {
            throw new Error(`ERROR SERVICE: in calling Data power service - Details: ${error}`);
        }
    }

}
