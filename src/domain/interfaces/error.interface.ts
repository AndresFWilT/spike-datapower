export interface IServiceError {
    StatusCode?: number;
    message: string;
}