import { Request } from 'express';
import { IncomingHttpHeaders } from 'http';

export interface IRequestHeadersDTO {
    'X-Channel': string;
    'X-Name': string;
    'X-RqUID': string;
    'Content-Type': string;
    'X-CustIdentNum'?: string;
    'X-CustIdentType'?: string;
}

export type IRequestHeadersCustom = IncomingHttpHeaders & IRequestHeadersDTO;

export interface IReqCustom<T = object> extends Request {
    body: T;
    headers: IRequestHeadersCustom;
}

export type IRequest = IReqCustom | Request;

export type IParamType = 'body' | 'headers';