export interface IUserQueryFilters {
    codTipoIdentificacion?: string;
    valNumeroIdentificacion?: string;
    codEmpresaAsociada?: string;
    valNombres?: string;
    valApellidos?: string;
    valTipoUsuario?: IUserClientType;
    valTipoAutenticacion?: IAuthType;
    valEstadoClave?: string;
    valDireccion?: string;
    valTelefono?: string;
    valCelular?: string;
    esNotificarCcvAlCorreo?: boolean;
    valTipoToken?: ITokenType;
    valPaisOrigen?: string;
    valCorreoElectronico?: string;
}

export type IUserClientType = 'BANCA_PERSONAL' | 'BANCA_EMPRESARIAL' | 'AUTOSERVICIOS';

export type IAuthType = 'TOKEN' | 'CCV';

export type ITokenType = 'SOFTWARE' | 'HARDWARE';

interface ISoapEnvelope {
    Body: SoapBody;
}

interface SoapBody {
    msjRespOpConsultarUsuariosPorFiltro: MsjRespOpConsultarUsuariosPorFiltro;
}

interface MsjRespOpConsultarUsuariosPorFiltro {
    contextoRespuesta: ContextoRespuesta;
    valNumeroUsuariosEncontrados: string;
    listaUsuarios: ListaUsuarios;
}

interface ContextoRespuesta {
    resultadoTransaccion: ResultadoTransaccion;
}

interface ResultadoTransaccion {
    codEstadoTransaccion: string;
    valCaracterAceptacion: string;
    valNumeroAprobacion: string;
    idTransaccion: string;
    fecOperacion: string;
}

interface ListaUsuarios {
    usuario: Usuario[];
}

interface Usuario {
    clienteNatural: ClienteNatural;
    telefonosExternos: TelefonosExternos;
    valPerfilAutenticacionUsuario: string;
    valTipoAutenticacion: string;
    valEstadoClaveVirtual: string;
    esNotificarAlCorreo: boolean;
    fecCreacion: string;
    fecActivacionCCV: string;
    fecCambioPIN: string;
}

interface ClienteNatural {
    codTipoIdentificacion: string;
    valNumeroIdentificacion: string;
    contacto: Contacto;
    valPrimerNombre: string;
    valSegundoNombre: string;
    valPrimerApellido: string;
    valSegundoApellido: string;
    codEmpresaAsociada: string;
}

interface Contacto {
    telefonos: Telefonos;
    correosElectronicos: CorreosElectronicos;
    ubicaciones: Ubicaciones;
}

interface Telefonos {
    telefono: Telefono[];
}

interface Telefono {
    codTipoTelefono: string;
    valNumeroTelefono: string;
}

interface CorreosElectronicos {
    valCorreoElectronico: string;
}

interface Ubicaciones {
    ubicacion: Ubicacion[];
}

interface Ubicacion {
    municipio: Municipio;
    valDireccion: string;
}

interface Municipio {
    departamento: Departamento;
}

interface Departamento {
    idDepartamento: string;
    codDepartamento: string;
    valNombre: string;
}

interface TelefonosExternos {
    telefonoExterno: TelefonoExterno[];
}

interface TelefonoExterno {
    valCodigoInternacional: string;
    valPrefijoPais: string;
    valNumeroTelefono: string;
}

interface ISoapErrorResponse {
    errorCode: string;
    errorMessage: string;
}

export type ISoapResponse = ISoapEnvelope | ISoapErrorResponse;