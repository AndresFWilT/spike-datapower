import { IUserQueryFilters, ISoapResponse } from '../interfaces';

export interface DataPowerRepository {
    fetchCustomerCrucialData(
        UUID: string,
        body: IUserQueryFilters,
        url: string,
        contract: string
    ): Promise<ISoapResponse>;
}
