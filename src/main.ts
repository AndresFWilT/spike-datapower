import { ServerConfiguration } from './infrastructure/server/server-configuration';
import { sendErrorResponse } from './adapters/filters/error-handler';
import router from './adapters/routes/datapower.route';

const server = new ServerConfiguration();
const app = server.app;
const fullApiPath = server.fullApiPath;

app.use(fullApiPath, router);
app.use(sendErrorResponse);

export default app;
