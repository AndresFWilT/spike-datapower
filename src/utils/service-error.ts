import { IServiceError } from '../domain/interfaces';

export function createServiceErrorResponse(error: IServiceError | Error): IServiceError {
    return {
        StatusCode: (error as IServiceError).StatusCode ?? 500,
        message: `ERROR SERVICE: in ${error.message}`,
    };
}

export function throwServiceError(message: string, statusCode: number = 500): IServiceError {
    const error: IServiceError = {
        StatusCode: statusCode,
        message,
    };
    throw error;
}