import { v4 as uuid } from 'uuid';

import { IReqCustom } from '../domain/interfaces';

/**
 * Retrieves the UUID associated with a request, generating a new one if none is present.
 *
 * If the 'X-RqUID' header is set and not empty, this value is returned as the UUID for the request.
 * If the 'X-RqUID' header is not set or empty, a new UUID is generated and returned.
 *
 * @param req The Express request object.
 * @return The UUID associated with the request.
 */
export function getRequestUUID(req: IReqCustom): string {
  let UUID = '';
  if (req.header('X-RqUID') !== '') {
    UUID = req.header('X-RqUID') as string;
  } else {
    UUID = uuid();
  }

  return UUID;
}