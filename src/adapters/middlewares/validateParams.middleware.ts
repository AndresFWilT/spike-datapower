import { NextFunction, Response } from 'express';

import { getRequestUUID, CustomError } from '../../utils';
import {
    IReqCustom,
    IRequest,
    IRequestHeadersDTO,
    IParamType
} from '../../domain/interfaces';

type ParamProvider = (req: IReqCustom) => string[];


const validateParams =
    (paramProvider: ParamProvider, paramType: IParamType) =>
        async (req: IRequest, _res: Response, next: NextFunction) => {
            const UUID = getRequestUUID(req as IReqCustom);
            const params = paramProvider(req as IReqCustom);

            const isParamValid = (param: string): boolean => {
                if (param === null || param === undefined) {
                    return false;
                }
                return true;
            };

            if (params.some((param) => !isParamValid(param))) {
                return next(new CustomError(UUID, 400, `${paramType}: parameters is not valid`));
            }

            next();
        };

export const validateRequestBody = validateParams((req) =>
    Object.values(req.body),
    'body'
);
export const validateRequestHeaders = validateParams((req) =>
    Object.values(req.headers as IRequestHeadersDTO),
    'headers'
);
