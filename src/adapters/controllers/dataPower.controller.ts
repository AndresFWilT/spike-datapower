import debugLib from 'debug';
import { NextFunction, Response, Router } from 'express';

import {
    IReqCustom,
    IRequest,
    IRequestHeadersCustom,
    ISoapResponse,
    IUserQueryFilters
} from '../../domain/interfaces';
import { DataPowerService } from '../../application/use-cases/dataPower.service';
import { CustomError, getRequestUUID } from '../../utils';

const debug = debugLib('davi:dataPower.controller');

export class DataPowerController {
    private readonly router = Router();

    constructor(
        private readonly dataPowerService: DataPowerService
    ) {
        this.router.get(
            '/data/connection',
            (req: IRequest, res: Response, next: NextFunction) => {
                this.getCustomerCrucialData(req, res, next).catch(next);
            }
        );
    }

    async getCustomerCrucialData(req: IRequest, res: Response, next: NextFunction) {
        const UUID = getRequestUUID(req as IReqCustom);
        try {
            const response: ISoapResponse = await this.dataPowerService.getIntentionAIResponse(
                UUID,
                req.body as IUserQueryFilters,
                req.headers as IRequestHeadersCustom
            );
            res.status(200).json({ ...response, });
        } catch (error: any) {
            const err = new CustomError(UUID, error.StatusCode, error.message);
            next(err);
        }
    }

    getRouter() {
        return this.router;
    }
}
