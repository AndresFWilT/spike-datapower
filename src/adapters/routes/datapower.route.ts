import { Router } from "express";

import {
    validateRequestBody,
    validateRequestHeaders,
} from '../middlewares';
import { DataPowerController } from "../controllers/dataPower.controller";
import { DataPowerService } from "../../application/use-cases/dataPower.service";
import { DataPowerImpl } from "../../infrastructure/server/integrations/DataPowerImpl.integration";

const router = Router();

const dataPowerSendDataController = new DataPowerController(
    new DataPowerService(
        new DataPowerImpl()
    )
)

router.use(
    [validateRequestHeaders, validateRequestBody],
    dataPowerSendDataController.getRouter()
)

export default router;