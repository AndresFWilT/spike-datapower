import { NextFunction, Request, Response } from 'express';
import debugLib from 'debug';
import { v4 as uuid } from 'uuid';

const debug = debugLib('davi:error-handler');

export function sendErrorResponse(
    error: Error & { status?: number; errors?: unknown[]; UUID: string },
    _request: Request,
    response: Response,
    _next: NextFunction,
): void {
    const status = error.status ?? 500;
    const currentDate = new Date().toISOString();
    let description: string;
    const _uuid: string = error.UUID || uuid();
    if (status === 500) {
        debug(`UUID: ${_uuid} - Response API core-data: complete exception - Status code: 500 - Message: ${error.message}`);
        description = 'Processing error';
    } else if (status === 400) {
        debug(`UUID: ${_uuid} - Response API core-data: complete exception - Status code: 400 - Message: ${error.message}`);
        description = 'Invalid Message Format';
    } else {
        debug(`UUID: ${_uuid} - Response API core-data: complete exception - Status code: unknown status code error.`);
        description = 'Unknown error';
    }

    const responseBody = {
        uuid: _uuid,
        StatusCode: status,
        Description: description,
        TimeStamp: currentDate,
    };
    debug(`UUID: ${_uuid} - Response getting core data: ${JSON.stringify(responseBody)}`);
    response.status(status).json(responseBody);
}
