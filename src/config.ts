export default {
    API_PATH: process.env.API_PATH || '/v1/product/',
    DEBUG: process.env.DEBUG || 'davi:*',
    PORT: process.env.PORT || '9084'
}